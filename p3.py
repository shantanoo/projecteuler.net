#!/opt/local/bin/python3.2

'''
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
'''

n = 600851475143

f = 2

while f ** 2 < n:
    while not n % f:
        n /= f
    f += 1

print(int(n))