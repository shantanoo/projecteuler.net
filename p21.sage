s = {}
ans = []

for x in xrange(1,10001):
    if x in Primes() or x in ans:
        continue
    try:
        x_div = divisors(x)[:-1]
        t = sum(x_div)
        t_div = divisors(t)[:-1]
        if x == t:
            continue
        if x == sum(t_div):
            print x,t
            ans.append(x)
            ans.append(t)
    except:
        pass
print 'Sum is: ', sum(ans)