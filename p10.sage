'''
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

'''

'''
Run following in sage (http://www.sagemath.org/)
'''


i = 0
s = 0
p = Primes()
while True:
    i = p.next(i)
    if i <= 2000000:
        s += i
    else:
        break

print s