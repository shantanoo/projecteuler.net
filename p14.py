#!/opt/local/bin/python3.2

'''
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
'''

d = {1:1}

def collatz(start):
    if start in d:
        return d[start]
    if start%2:
        d[start] = 1 + collatz(3*start + 1)
        return d[start]
    else:
        d[start] = 1 + collatz(start//2)
        return d[start]

t = [0,0]
for x in range(1,1000000):
    z = collatz(x)
    if z > t[1]:
        t = [x, z]

print(t[0])