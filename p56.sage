'''
A googol (10^100) is a massive number: one followed by one-hundred zeros; 
100^100 is almost unimaginably large: one followed by two-hundred zeros. 
Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, ab, where a, b < 100, 
what is the maximum digital sum?
'''

# Funciton m could be replaced by reduce function
def m(i):
    y = 0
    for x in str(i):
        y += int(x)
    return y

c = 0
for a in range(100):
    for b in range(100):
        c = max(c, m(a**b))
        
print c