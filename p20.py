#!/opt/local/bin/python3.2

'''
n! means n × (n − 1) × ... × 3 × 2 × 1

Find the sum of the digits in the number 100!

'''

from functools import reduce
print(sum([int(x) for x in str(reduce(lambda a,b: a*b, range(1,101)))]))