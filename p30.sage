def sum_power(n, p):
    return sum(int(x)**p for x in str(n))
    
s = 0
p = 5
for x in xrange(2, (9*9**p)+1):
    if x == sum_power(x,p):
        print x
        s += x

print 'Sum: ', s