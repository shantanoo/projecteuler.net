#!/opt/local/bin/python3.2

'''
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.


'''

from sys import exit

for x in range(1,500):
    for y in range(x+1,500):
        z =(x**2 + y**2)**.5 
        if  z == int(z) and x+y+z==1000:
            print(int(x*y*z))
            exit(0)