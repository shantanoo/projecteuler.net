p = Primes()
l = [2]
c = set()

def check_cirular(no):
    if '0' in str(no):
        return False
    flag = 0
    r = []
    for i in range(len(str(no))):
        no = str(no)
        no = no[1:] + no[0]
        no = int(no)
        if no in Primes():
            flag += 1
            r.append(no)
        else:
            return False
    return r

while l[-1] < 1000000:
    l.append(p.next(l[-1]))

l = set(l)

for x in l:
    if x not in c and check_cirular(x):
        c.add(x)

c = list(c)
c.sort()
print c
print 'Total:', len(c)