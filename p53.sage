def ncr(n,r):
    ret = factorial(n)/(factorial(r)*factorial(n-r))
    return ret

s = 0
for n in xrange(1,101):
    for r in range(n):
        if ncr(n,r) > 10**6:
            s += 1

print s